#!/usr/bin/env bash

set -Eeuo pipefail;

if [ "$#" -lt 3  ]; then
    echo 'Check number of arguments and try again'
    echo 'Hint: scripts/run_bazel.sh bash to get shell'
    exit 1
fi

if [ "$3" = 'bash' ]; then
    exec bash
fi

exec /usr/local/bin/bazel "$@"
