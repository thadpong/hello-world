#!/usr/bin/env bash

STABLE_GIT_HASH=$(git rev-parse HEAD)

if [ "$?" -ne 0 ]; then
    exit 1
fi

echo "BUILD_TIMESTAMP_HUMAN $(date -u +'%Y-%m-%dT%H:%M:%SZ')"
echo "STABLE_GIT_HASH ${STABLE_GIT_HASH}"