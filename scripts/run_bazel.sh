#!/usr/bin/env bash

BASEDIR=$(cd "$(dirname "$0")"; pwd -P)
ROOT_DIR=$(dirname "$BASEDIR")
SRC_DIR=$ROOT_DIR/sources
OUTPUT_DIR=/tmp/.build

LOCAL_DOCKER_SOCKET="/var/run/docker.sock"
REMOTE_DOCKER_SOCKET="/var/run/docker.sock"

echo "Root path = $ROOT_DIR"
echo "Work space = $PWD"

mkdir -p $OUTPUT_DIR

# Since we need to manipulate docker, we need to mount docker socket into container
# and need root user to control socket; Need to find a way to do this without root
docker run \
  -it \
  --rm \
  --entrypoint=/entrypoint.sh \
  -v "$ROOT_DIR/scripts/entrypoint.sh:/entrypoint.sh":ro \
  -v "$ROOT_DIR:$ROOT_DIR" \
  -v "$OUTPUT_DIR:$OUTPUT_DIR" \
  -v "$REMOTE_DOCKER_SOCKET:$LOCAL_DOCKER_SOCKET" \
  -w "$PWD" \
  l.gcr.io/google/bazel:2.2.0 \
  --max_idle_secs=5 \
  --output_user_root=$OUTPUT_DIR \
  "$@"
