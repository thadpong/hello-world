# Terraform Local Tutorial

Local deployment steps :
1. Install terraform cli.
2. Run "terraform init" to initialize.
3. Run "terraform apply" to provision the system.
4. Run "docker ps" to verify that the system is running successfully.
5. Run "terraform destroy" to destroy the system.

For more information, please visit https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/aws-get-started .
