# Terraform AWS Tutorial

AWS deployment steps :
1. Install terraform cli and aws cli along with its credential (via "aws configure").
2. Run "terraform init" to initialize.
3. Run "terraform fmt" and "terraform validate" to check the config file format.
3. Run "terraform apply" to provision the system.
4. Run "terraform show" and "terraform state list" to verify that the system is running successfully.
5. Run "terraform destroy" to destroy the system.

For more information, please visit https://learn.hashicorp.com/tutorials/terraform/aws-build?in=terraform/aws-get-started .
