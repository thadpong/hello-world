package router

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

type svcRoute struct {
	Method  string
	Path    string
	Handler echo.HandlerFunc
}

func GetRoute(e *echo.Echo, nearbysearch, linebot echo.HandlerFunc) *echo.Echo {

	route := getV1(nearbysearch, linebot)

	g := e.Group("/api/v1")

	for _, r := range route {
		g.Add(r.Method, r.Path, r.Handler)
	}

	return e

}

// Return route for service endpoints
func getV1(nearbysearch, linebot echo.HandlerFunc) []svcRoute {

	r := []svcRoute{
		{
			Method:  http.MethodPost,
			Path:    "/nearbysearch",
			Handler: nearbysearch,
		},
		{
			Method:  http.MethodPost,
			Path:    "/echobot",
			Handler: linebot,
		},
	}

	return r
}
