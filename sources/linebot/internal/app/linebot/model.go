package linebot

type DBSchema struct {
	UserID  string `json:"userID" bson:"user_id"`
	Message string `json:"message" bson:"message"`
}
