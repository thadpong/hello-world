package linebot

import (
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
	line "github.com/line/line-bot-sdk-go/linebot"
	"linebot/internal/pkg/db"
	"log"
	"net/http"
)

type Handler struct {
	Client  *line.Client
	Mongo   *db.MongoClient
	LineLog *log.Logger
}

const (
	DBNAME       = "linebot"
	DBCOLLECTION = "message"
)

// Sample endpoint for storing userid and message into mongo and send user message back
func (p *Handler) Endpoint(c echo.Context) error {

	events, err := p.Client.ParseRequest(c.Request())
	if err != nil {
		return c.NoContent(http.StatusInternalServerError)
	}

	for _, e := range events {
		if e.Type == line.EventTypeMessage {
			switch message := e.Message.(type) {
			case *line.TextMessage:

				doc, err := bson.Marshal(DBSchema{
					Message: message.Text,
					UserID:  e.Source.UserID,
				})
				if err != nil {
					p.LineLog.Printf("error: %+v", err)
					return c.NoContent(http.StatusInternalServerError)
				}

				err = p.Mongo.Insert(DBNAME, DBCOLLECTION, doc)
				if err != nil {
					p.LineLog.Printf("error: %+v", err)
					return c.NoContent(http.StatusInternalServerError)
				}

				_, err = p.Client.ReplyMessage(e.ReplyToken, line.NewTextMessage(message.Text)).Do()
				if err != nil {
					p.LineLog.Printf("error: %+v", err)
					return c.NoContent(http.StatusInternalServerError)
				}
			}
		}
	}

	return c.NoContent(http.StatusOK)
}
