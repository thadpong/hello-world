package config

import "os"

type Config struct {
	// App config
	Port      string `mapstructure:"port"`
	MongoAddr string `mapstructure:"mongo_address"`
	MongoPort string `mapstructure:"mongo_port"`
	MongoDB   string `mapstructure:"mongo_database"`
	MongoCol  string `mapstructure:"mongo_collection"`

	// Linebot config
	ChannelSecret string `mapstructure:"channel_secret"`
	ChannelToken  string `mapstructure:"channel_token"`

	// Google Map config
	GoogleAPIKey   string `mapstructure:"google_api_key"`
	GoogleEndpoint string `mapstructure:"google_endpoint"`
}

// Sample config
func New() *Config {
	port, ok := os.LookupEnv("PORT")
	if !ok {
		port = "8080"
	}

	mongoAddr, ok := os.LookupEnv("MONGO_ADDR")
	if !ok {
		mongoAddr = "localhost"
	}

	mongoPort, ok := os.LookupEnv("MONGO_PORT")
	if !ok {
		mongoPort = "27017"
	}

	mongoDB, ok := os.LookupEnv("MONGO_DB")
	if !ok {
		mongoDB = "linebot"
	}

	mongoCol, ok := os.LookupEnv("MONGO_COL")
	if !ok {
		mongoCol = "message"
	}

	chanSecret, ok := os.LookupEnv("CHANNEL_SECRET")
	if !ok {
		chanSecret = "LINE_CHANNEL_SECRET"
	}

	chanToken, ok := os.LookupEnv("CHANNEL_TOKEN")
	if !ok {
		chanToken = "LINE_CHANNEL_TOKEN"
	}

	googleKey, ok := os.LookupEnv("GOOGLE_API_KEY")
	if !ok {
		googleKey = "GOOGLE_API_KEY"
	}

	googleEndpoint, ok := os.LookupEnv("google_endpoint")
	if !ok {
		googleEndpoint = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
	}

	return &Config{
		Port:           port,
		MongoAddr:      mongoAddr,
		MongoPort:      mongoPort,
		MongoDB:        mongoDB,
		MongoCol:       mongoCol,
		ChannelSecret:  chanSecret,
		ChannelToken:   chanToken,
		GoogleAPIKey:   googleKey,
		GoogleEndpoint: googleEndpoint,
	}
}
