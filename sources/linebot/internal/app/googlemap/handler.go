package googlemap

import (
	"context"
	"errors"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"time"
)

const (
	CallTimeout = time.Second * 5
)

type NearbySearchHandler struct {
	NBCaller *NearbySeachCaller
	NBLog    *log.Logger
}

func (p *NearbySearchHandler) Endpoint(c echo.Context) error {
	r := &Request{}
	if err := c.Bind(r); err != nil {
		return c.NoContent(http.StatusBadRequest)
	}

	ctx, cancel := context.WithTimeout(context.TODO(), CallTimeout)
	defer cancel()

	res, err := p.NBCaller.ProvideNearbySeach(ctx, &r)
	if err != nil {
		p.NBLog.Printf("error: %+v", err)
		return c.JSON(http.StatusInternalServerError, errors.New("failed to call google map api"))
	}

	return c.JSON(http.StatusOK, res.JsonResult)
}
