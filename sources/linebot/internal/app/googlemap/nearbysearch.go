package googlemap

import (
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type NearbySeachProvider interface {
	ProvideNearbySeach(ctx context.Context, request interface{}) (Response, error)
}

type NearbySeachCaller struct {
	APIEndpoint string
	APIKey      string
	Client      *http.Client
	NBCallerLog *log.Logger
}

func NewPlaceSeachProvider(endpoint, key string, cl *http.Client, log *log.Logger) *NearbySeachCaller {
	return &NearbySeachCaller{
		APIEndpoint: endpoint,
		APIKey:      key,
		Client:      cl,
		NBCallerLog: log,
	}
}

func (t *NearbySeachCaller) ProvideNearbySeach(ctx context.Context, request interface{}) (Response, error) {
	r, ok := request.(*Request)
	if !ok {
		err := errors.New("bad request")
		t.NBCallerLog.Printf("error: %+v", err)
		return Response{}, err
	}

	result, err := t.queryAllResult(r)
	if err != nil {
		t.NBCallerLog.Printf("error: %+v", err)
		return Response{}, err
	}

	return Response{
		JsonResult: strings.Join(result, ","),
	}, nil
}

// Todo: Refactor this method
func (t *NearbySeachCaller) queryAllResult(request *Request) ([]string, error) {
	var nextPageToken string
	var tmpRes []string

	params := url.Values{}
	params.Set("key", t.APIKey)
	params.Set("location", request.Location)
	params.Set("radius", request.Radius)

	// Map api allow 3 subsequent search
	for queryRnd := 1; queryRnd < 3; queryRnd++ {
		if nextPageToken != "" {
			params.Set("pagetoken", nextPageToken)
		}

		req, err := http.NewRequest(http.MethodGet, t.APIEndpoint, nil)
		if err != nil {
			t.NBCallerLog.Printf("error: %+v", err)
			return nil, errors.New("failed to create new place search request")
		}

		req.URL.RawQuery = params.Encode()

		res, err := t.Client.Do(req)
		if err != nil {
			t.NBCallerLog.Printf("error: %+v", err)
			return nil, err
		}

		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			t.NBCallerLog.Printf("error: %+v", err)
			return nil, err
		}

		var partialRes *GoogleResponse
		if partialRes, err = handleResponse(body); err != nil {
			t.NBCallerLog.Printf("error: %+v", err)
			return nil, err
		}

		switch partialRes.Status {
		case "OK":
			tmpRes = append(tmpRes, partialRes.Results...)
			if _, found := partialRes.TheRest["next_page_token"]; !found {
				t.NBCallerLog.Printf("error: %+v", err)
				return tmpRes, err
			}

			nextPageToken = partialRes.TheRest["next_page_token"].(string)

		default:
			return nil, errors.New("failed response from map api")
		}

		time.Sleep(time.Second)

	}

	return tmpRes, nil

}

func handleResponse(res []byte) (*GoogleResponse, error) {
	googRes := GoogleResponse{}
	if err := json.Unmarshal(res, &googRes); err != nil {
		return &GoogleResponse{}, err
	}
	if err := json.Unmarshal(res, &googRes.TheRest); err != nil {
		return &GoogleResponse{}, err
	}
	delete(googRes.TheRest, "Status")
	delete(googRes.TheRest, "Results")

	return &googRes, nil
}
