package googlemap

type (
	Request struct {
		Location string `json:"location" bson:"location"`
		Radius   string `json:"radius" bson:"radius"`
		Type      string `json:"type" bson:"type"`
	}

	Response struct {
		JsonResult string `json:"jsonResult" bson:"jsonResult"`
	}

	GoogleResponse struct {
		Status  string                 `json:"status"`
		Results []string               `json:"results"`
		TheRest map[string]interface{} `json:"-"`
	}
)
