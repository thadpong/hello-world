package app

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/labstack/echo/v4"

	"github.com/labstack/echo/v4/middleware"
)

type ApiHandler interface {
	Endpoint(c echo.Context) error
}

const shutdownTimeout = time.Second * 10

func GetDefaultRouter() *echo.Echo {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/health", func(ctx echo.Context) error {
		return ctx.NoContent(http.StatusOK)
	})

	return e
}

type Server struct {
	server *http.Server
	srvlog *log.Logger
}

func CreateNewServer(port string, router http.Handler, logger *log.Logger) *Server {

	return &Server{
		server: &http.Server{
			Addr:    port,
			Handler: router,
		},
		srvlog: logger,
	}
}

func (s *Server) StartServer() error {

	go func() {
		if err := s.server.ListenAndServe(); err != nil {
			s.srvlog.Fatalf("Failed to start server: %+v", err)
		}
	}()

	s.srvlog.Printf("Server start listening at port %+v", s.server.Addr)

	if err := s.GracefulShutdown(); err != nil {
		s.srvlog.Fatalf("Failed to gracefully shutdown server: %+v", err)
	}

	return nil
}

func (s *Server) GracefulShutdown() error {

	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, syscall.SIGTERM, syscall.SIGINT)

	<-sigint

	ctx, cancel := context.WithTimeout(context.TODO(), shutdownTimeout)
	defer cancel()

	if err := s.server.Shutdown(ctx); err != nil {
		s.srvlog.Fatalf("Failed to gracefully shutdown server: %+v", err)
	}

	return nil
}
