package db

import (
	"crypto/tls"
	"errors"
	"net"
	"time"

	"github.com/globalsign/mgo"
)

var ErrClientUninitialized = errors.New("MongoDB's connection has not been initialized")

type (
	Client interface {
		Insert(db, collection string, data interface{}) error
		Update(db, collection string, query, into interface{}) error
		Search(db, collection string, query, result interface{}) error
		SearchOne(db, collection string, query, result interface{}) error
	}

	MongoClient struct {
		session *mgo.Session
	}
)

func NewMongoClient(address []string, username, password string, replicaset string, authDB string, timeout time.Duration) (*MongoClient, error) {
	dialInfo := &mgo.DialInfo{
		Addrs:          address,
		Timeout:        timeout,
		Username:       username,
		Password:       password,
		ReplicaSetName: replicaset,
		Source:         authDB,
	}

	tlsConfig := &tls.Config{}

	dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
		conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
		return conn, err
	}

	s, err := mgo.DialWithInfo(dialInfo)
	if err != nil {
		return nil, err
	}
	s.SetMode(mgo.Monotonic, true)

	return &MongoClient{
		session: s,
	}, nil
}

func (c *MongoClient) Search(db, collection string, query, result interface{}) error {
	s := c.session.Copy()
	if s == nil {
		return ErrClientUninitialized
	}
	defer s.Close()

	return s.DB(db).C(collection).Find(query).All(result)
}

func (c *MongoClient) SearchOne(db, collection string, query, result interface{}) error {
	s := c.session.Copy()
	if s == nil {
		return ErrClientUninitialized
	}
	defer s.Close()

	return s.DB(db).C(collection).Find(query).One(result)
}

func (c *MongoClient) Insert(db, collection string, data interface{}) error {
	s := c.session.Copy()
	if s == nil {
		return ErrClientUninitialized
	}
	defer s.Close()

	return s.DB(db).C(collection).Insert(data)
}

func (c *MongoClient) InsertAll(db, collection string, list []interface{}) error {
	s := c.session.Copy()
	if s == nil {
		return ErrClientUninitialized
	}
	defer s.Close()

	bulk := s.DB(db).C(collection).Bulk()
	for _, item := range list {
		bulk.Insert(item)
	}

	_, err := bulk.Run()
	return err
}

func (c *MongoClient) Remove(db, collection string, query interface{}) error {
	s := c.session.Copy()
	if s == nil {
		return ErrClientUninitialized
	}
	defer s.Close()

	return s.DB(db).C(collection).Remove(query)
}

func (c *MongoClient) RemoveAll(db, collection string, query interface{}) error {
	s := c.session.Copy()
	if s == nil {
		return ErrClientUninitialized
	}
	defer s.Close()

	_, err := s.DB(db).C(collection).RemoveAll(query)
	return err
}

func (c *MongoClient) Update(db, collection string, query, into interface{}) error {
	s := c.session.Copy()
	if s == nil {
		return ErrClientUninitialized
	}
	defer s.Close()

	return s.DB(db).C(collection).Update(query, into)
}

func (c *MongoClient) UpdateAll(db, collection string, query, into interface{}) error {
	s := c.session.Copy()
	if s == nil {
		return ErrClientUninitialized
	}
	defer s.Close()

	_, err := s.DB(db).C(collection).UpdateAll(query, into)
	return err
}

func (c *MongoClient) Close() {
	if c.session != nil {
		c.session.Close()
		c.session = nil
	}
}
