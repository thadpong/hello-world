# Sample linebot in golang

In this sample project, we will implement a simple linebot using line golang sdk.

## Functionality:

    - Perform nearby searching with GoogleMap API
    - Simple echo bot

## Building:

    - Using the provided Dockerfile inside the docker directories
    - Or use bazel

## Note:

    - The external service has not been tested since there is no API key
    - Build as a testbed project for Bazel build tools
