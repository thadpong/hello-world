package main

import (
	"fmt"
	line "github.com/line/line-bot-sdk-go/linebot"
	"linebot/internal/app"
	"linebot/internal/app/config"
	"linebot/internal/app/googlemap"
	"linebot/internal/app/linebot"
	"linebot/internal/app/router"
	"linebot/internal/pkg/db"
	"log"
	"net/http"
	"os"
	"time"
)

const (
	CALLERTIMEOUT = time.Second * 5
	MONGOTIMEOUT  = time.Second * 5
)

func initAPP() (*app.Server, error) {

	cfg := config.New()

	nb := googlemap.NewPlaceSeachProvider(
		cfg.GoogleEndpoint,
		cfg.GoogleAPIKey,
		&http.Client{
			Timeout: CALLERTIMEOUT,
		},
		log.New(os.Stderr, "NearbySearch-caller: ", log.LstdFlags|log.Lshortfile),
	)

	ps := googlemap.NearbySearchHandler{
		NBCaller: nb,
		NBLog:    log.New(os.Stderr, "NearbySearch-Handler: ", log.LstdFlags|log.Lshortfile),
	}

	bot, err := line.New(
		cfg.ChannelSecret,
		cfg.ChannelToken,
	)
	if err != nil {
		panic(err)
	}

	mg, err := db.NewMongoClient([]string{cfg.MongoAddr}, "", "", "", "", MONGOTIMEOUT)
	if err != nil {
		panic(err)
	}

	lb := linebot.Handler{
		Client:  bot,
		Mongo:   mg,
		LineLog: log.New(os.Stderr, "Linebot-Handler: ", log.LstdFlags|log.Lshortfile),
	}

	e := router.GetRoute(app.GetDefaultRouter(), ps.Endpoint, lb.Endpoint)
	s := app.CreateNewServer(fmt.Sprintf(":%s", cfg.Port), e, log.New(os.Stderr, "Server: ", log.LstdFlags|log.Lshortfile))

	return s, nil

}

func main() {
	s, err := initAPP()

	if err != nil {
		panic(err)
	}

	_ = s.StartServer()
}
