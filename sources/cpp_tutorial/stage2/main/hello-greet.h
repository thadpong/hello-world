#ifndef _CPPTUTORIAL_STAGE2_LIB_HELLO_GREET_H_
#define _CPPTUTORIAL_STAGE2_LIB_HELLO_GREET_H_

#include <string>

std::string get_greet(const std::string &thing);

#endif
