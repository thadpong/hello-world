#include "gtest/gtest.h"
#include "hello-greet.h"

TEST(HelloGreetTest, ReturnHello) {
  EXPECT_EQ("Hello2 ", get_greet(""));
  EXPECT_EQ("Hello2 world", get_greet("world"));
  EXPECT_EQ("Hello2 me", get_greet("me"));
}
