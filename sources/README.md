# Bazel with Golang

## Setup
    - Install bazel into your environment or use the convenient script `run_bazel.sh`

## Assumption
    - System with docker installed 
    - Linux
    - Internet access
    - Go module development workflow

## How to prepare go project with Bazel
    - Code your golang project as usual
    - Create a new BUILD file at your project root
    - Edit the BUILD file
        - Update `# gazelle:prefix $MODULE_NAME` to your module name
        - Run `bazel run sources/linebot:gazelle -- update-repos --from_file=sources/linebot/go.mod -prune=true`
        - Run `bazel run sources/linebot:gazelle`
        - The go_binary will be create for this project

## How to run with Bazel
    - Run `bazel run sources/linebot:linebot` to run binary

## How to save docker image with Bazel
    - Run `bazel build -c dbg sources/linebot:image.tar` to save docker image with debug tool
    - Run `bazel build -c opt sources/linebot:image.tar` to save docker image without debug tool

## How to clean up
    - Run `bazel clean`
