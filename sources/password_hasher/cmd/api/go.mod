module example.com/password_hasher

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	golang.org/x/crypto v0.0.0-20200423211502-4bdfaf469ed5
)
